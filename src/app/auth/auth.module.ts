import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxAuthRoutingModule } from './auth-routing.module';
import { NbAuthModule, NbPasswordAuthStrategy, NbAuthJWTToken } from '@nebular/auth';
import { 
  NbAlertModule,
  NbButtonModule,
  NbCheckboxModule,
  NbInputModule,
  NbSpinnerModule
} from '@nebular/theme';


import { NgxLoginComponent } from './login/login.component'; 
import { environment } from '../../environments/environment';

const formSetting: any = {
    //redirectDelay: 0,
    showMessages: {
      success: true,
    },
  };

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NgxAuthRoutingModule,

    NbSpinnerModule,

    NbAuthModule.forRoot({
        strategies: [
          
        ],
        forms: {},
      }), 
  ],
  declarations: [
    NgxLoginComponent
  ],
})
export class NgxAuthModule {
}