import { Component } from '@angular/core';
import { NbAuthSocialLink } from '@nebular/auth';
import { LoginService } from './service/login.service';
import { NbToastrService, NbToastrConfig } from '@nebular/theme';
import { BeatmToastrService } from '../../services/beatm.toastr.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent {

  loading: Boolean = false;

    redirectDelay: number;
    showMessages: any;
    strategy: string;
    errors: string[];
    messages: string[];
    submitted: boolean;
    socialLinks: NbAuthSocialLink[];
    rememberMe: boolean;

    usuario = {
      email: '',
      password: '',
      rememberMe: true
    };

    constructor(private service: LoginService, private toastr: BeatmToastrService) {
      
    }

    login() {
      //this.submitted = true;
      this.loading = true;
      var request = this.service.login(this.usuario);
      request.subscribe((response) => {
        this.loading = false; 
        var data: any = response['body'];
        if ((data !== null && data !== undefined) && !data.success) {
          this.toastr.warning('Dados incorretos', 'Email/Senha incorretos. Verifique seus dados.');
        } else {
          var token = response.headers.get("Authorization");
          if (token !== null && token !== undefined) {
            localStorage.setItem("token", token);
            this.me();
          }
        }

        console.log(localStorage.getItem("token"));
      });
    }

    me() {
      var request = this.service.me();
      request.subscribe((response) => {
        var data: any = response['body'];
        localStorage.setItem("usuario", JSON.stringify(data));
        this.toastr.success('Login efetuado', 'Usuário logado com email <b>' + data.usuario.email + '</b>');
        setTimeout(() => {
          window.location.href='/';
        }, 2500);
      });
    }

    logout() {
      var request = this.service.logout();
      request.subscribe((response) => {
        var data: any = response['body'];
        localStorage.removeItem("token");
        window.location.href='/auth/login';
      });
    }

    teste(texto: string) {
      alert(texto);
    }
}