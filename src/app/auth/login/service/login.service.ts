import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    host: any = environment.api;

    HttpConfig = {
      headers: {
        "Content-type": "application/json"
      }
    }  

    constructor(private httpClient: HttpClient) {
        
    }
    

    public login(data: Object) {
      let request = this.httpClient.post(this.host + '/auth/login', data, {headers: this.HttpConfig.headers, observe: 'response'});
      return request;
    }

    public me() {
      this.setToken();
      let request = this.httpClient.post(this.host + '/auth/me', {}, {headers: this.HttpConfig.headers, observe: 'response'});
      return request;
    }

    public logout() {
      this.setToken();
      let request = this.httpClient.post(this.host + '/auth/logout', {}, {headers: this.HttpConfig.headers, observe: 'response'});
      return request;
    }

    private setToken(): void {
      let token = localStorage.getItem("token");
      if (token !== null && token !== undefined && token !== '') {
        this.HttpConfig.headers['Authorization'] = token;
      }      
    }
}