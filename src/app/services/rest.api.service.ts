import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {

    host: any = environment.api;

    httpOptions = {
        headers: {
          'Content-Type': 'application/json'
        }
      }  

    constructor(private httpClient: HttpClient) {
        var token = localStorage.getItem("token");
        if (token !== null && token !== undefined) {
            this.httpOptions.headers['Authorization'] =  token;
        }
    }
    
    public post(url: string, data: Object) {
        var request = this.httpClient.post(this.host + url, data, {headers: this.httpOptions.headers, observe: 'response'});
        return request.subscribe((response) => {
            var dados = JSON.parse(JSON.stringify(response.body));
            if (dados.code === 200 && 
                        ((dados.mensagem === null) || 
                            (dados.mensagem === undefined) || 
                                (dados.mensagem) === '')) {
                return dados;
            } else {
                this.errorHandler(dados);
            }
        });
    }

    public get(url: string) {
        var request = this.httpClient.get(this.host + url, {headers: this.httpOptions.headers, observe: 'response'});
        return request.subscribe((response) => {
            var dados = JSON.parse(JSON.stringify(response.body));
            if (dados.code === 200 && 
                        (dados.mensagem === null) || 
                            (dados.mensagem === undefined) || 
                                (dados.mensagem) === '') {
                return request;
            } else {
                this.errorHandler(dados);
            }
        });
    }

    public errorHandler(error) {
        alert(error.mensagem);
    } 
}