import { Injectable } from '@angular/core';
import { NbToastrService } from '@nebular/theme';

@Injectable({
    providedIn: 'root'
})
export class BeatmToastrService {

    constructor(private toastr: NbToastrService) {
       
    }
    
    public success(titulo: string, mensagem: string): void {
        this.toastr.show(mensagem, titulo, 
                                                {
                                                    'status': 'success',
                                                    'duration': 3000,
                                                    'destroyByClick': true
                                                 });


    }

    public warning(titulo: string, mensagem: string): void {
        this.toastr.show(mensagem, titulo, 
                                                {
                                                    'status': 'warning',
                                                    'duration': 3000,
                                                    'destroyByClick': true
                                                 });

                                                 
    }

    public danger(titulo: string, mensagem: string): void {
        this.toastr.show(mensagem, titulo, 
                                                {
                                                    'status': 'danger',
                                                    'duration': 3000,
                                                    'destroyByClick': true
                                                 });

                                                 
    }


}