import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterLink, ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../../auth/login/service/login.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';

  userMenu = [ 
      { 
        title: 'Perfil', 
        icon: 'person'
      }, 
      { 
        title: 'Sair', 
        icon: 'log-out'
      } 
  ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private userService: UserData,
              private breakpointService: NbMediaBreakpointsService,
              private sanitizer: DomSanitizer,
              private router: Router,
              private loginService: LoginService) {
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;

    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => this.user = users.nick);

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);

    this.menuService.onItemClick().subscribe((item) => {
      let title = item.item.title.toLowerCase();
      if (title.indexOf('perfil') !== -1) {
        this.router.navigate(['/pages/profile']);
      }
      if (title.indexOf('sair') !== -1) {
        let request = this.loginService.logout();
        request.subscribe((response) => {
          let data = response['body'];
          if (data === true) {
            localStorage.removeItem("token");
            this.router.navigate(['/auth/login']);
          }          
        })
      }
    })

    let token = localStorage.getItem("token");
    if (token === null || token === undefined || token === "") {
      let location = window.location.href;
      if (location.toString().indexOf('login') === -1) {
        window.location.href = '/auth/login';
      }
    } else {
      let usuario = localStorage.getItem("usuario");
      usuario = JSON.parse(usuario);

      this.user = usuario;
      this.user.foto = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.user.foto);
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  teste() {
    alert("teste");
  }
}
